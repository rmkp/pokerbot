from deuces import Card
import random

class Player:

	def __init__(self, chips = None, hand = None, auto_draw = False, deck = None):
		if (chips == None):
			self.chips = 500
		else: 
			self.chips = chips
		self.hand = hand

		names = [l.split()[0] for l in open("names.txt").readlines() if len(l.split()[0]) == 6]
		self.name = random.choice(names)

		if (auto_draw and deck != None):
			self.draw(deck, 2)

	def __str__(self):
		ret = self.name + " - " + str(self.chips) + " chips & "

		if (self.hand != None):
			ret += Card.as_str(self.hand)
		return ret

	def draw(self, deck, num_cards):
		self.hand = deck.draw(num_cards)
			