from deuces import Card, Evaluator, Deck
from operator import itemgetter
import random
random.seed(123)

deck = Deck() #print Card.print_pretty_cards(deck.cards)
e = Evaluator()

# global variables
num_players = 4
sim_size = 10000

players = [deck.draw(2) for i in xrange(num_players)]

print 'players:'
for idx, h in enumerate(players):
	card_as_str = ''
	for c in h:
		card_as_str += Card.int_to_pretty_str(c)
	print idx, card_as_str

'''
Given a current board (3-5 cards) + some number of players' starting hands, will return a list of winners (in the case of
a tie), or more typically, a list of one list is made up of tuples: (idx of winning player, hand rank, hand name)
'''
def winners(current_board, starting_hands):
	hand_scores = [e.evaluate(current_board, h) for h in starting_hands]
	hand_names = [e.class_to_string(e.get_rank_class(hs)) for hs in hand_scores]
	hands_stats = [(idx,score,name) for idx,score, name in zip(xrange(len(hand_scores)),hand_scores,hand_names)]
	hands_stats_sorted = sorted(hands_stats, key=itemgetter(1))
	#print hands_stats_sorted
	best = hands_stats_sorted[0][1]
	ret = [h for h in hands_stats_sorted if h[1] == best]
	return ret

'''
Given a current board (3-5 cards) + some number of players' starting hands, this function finishs out the hand, checks
the winner, and repeats this process many times.  It then returns a list of win percentages for each player
'''
def win_percents(current_board, starting_hands, num_sims):
	total_wins = 0
	player_wins = [0 for i in xrange(len(starting_hands))]
	to_draw = 5 - len(current_board)

	for i in xrange(num_sims):
		if (to_draw != 1):
			other_cards = deck.draw_rand(to_draw)
		else:
			other_cards = [deck.draw_rand(to_draw)]
		final_board = other_cards + current_board
		who_won = winners(final_board, starting_hands)
		for w in who_won:
			player_wins[w[0]] += 1
			total_wins += 1
		deck.return_cards(other_cards) # needed for sim... otherwise we won't have the full deck for next trial

	return [(idx,p*1.0/total_wins) for idx, p in zip(xrange(len(starting_hands)),player_wins)]

print 'board: (flop)'
board = deck.draw(3)
Card.print_pretty_cards(board)
print win_percents(board, players, sim_size)

print 'board: (turn)'
turn = [deck.draw()]
board = board + turn
Card.print_pretty_cards(board)
print win_percents(board, players, sim_size)

print 'board: (river)'
river = [deck.draw()]
board = board + river
Card.print_pretty_cards(board)
print win_percents(board, players, sim_size)











