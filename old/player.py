import random
from hand import *

class Player:

	def __init__(self, chips = None, hand = None):
		if (chips == None):
			self.chips = 500
		else: 
			self.chips = chips
		self.hand = hand

		names = [l.split()[0] for l in open("names.txt").readlines() if len(l.split()[0]) == 6]
		self.name = random.choice(names)

	def __str__(self):
		ret = self.name + " - " + str(self.chips) + " chips & "
		ret += str(self.hand)
		return ret

	def draw(self, deck, num_cards):
		self.hand = Hand()
		for i in xrange(num_cards):
			self.hand.append(deck.draw())
			