import collections

class Hand:

	def __init__(self, cards = None):
		if (cards == None):
			self.cards = []
		else:
			self.cards = cards

	def append(self, card):
		self.cards.append(card)

	def count(self):
		return len(self.cards)

	def __str__(self):
		print_str = ''
		for c in self.cards:

			if (len(str(c)) == 3):
				print_str += str(c) + ' '
			else: 
				print_str += ' ' + str(c) + ' '

		return print_str

	def __getitem__(self,idx):
		return self.cards[idx]

	def suits(self):
		return [c.suit for c in self.cards]

	def ranks(self):
		return [c.rank for c in self.cards]

	def rankings(self):
		ret = []
		ret_names = []
		ranks_inc_amount = int(1e6)
		if (self.count() != 5):
			ret.append(-1)
		else:
			ranks_inc = 0
			counter = collections.Counter(self.ranks())
			counter2 = collections.Counter(self.suits())

			# high card
			high_card = sum([2**rank for rank in self.ranks()]) + ranks_inc
			ret.append(high_card)
			ret_names.append('high_card')
			ranks_inc += int(ranks_inc_amount)

			# one pair
			if (counter.most_common(1)[0][1] == 2):
				_type = counter.most_common(1)[0][0]
				rest_of_hand = set(self.ranks()) - set([_type])
				total_val = _type*(2**13) + ranks_inc + sum([2**rank for rank in rest_of_hand])
				ret.append(total_val)
				ret_names.append('one pair')
			ranks_inc += int(ranks_inc_amount)

			# two pair
			if (counter.most_common(2)[0][1] == 2 and counter.most_common(2)[1][1] == 2):
				_type1 = counter.most_common(1)[0][0] 
				_type2 = counter.most_common(2)[1][0]
				_type3 = counter.most_common(3)[2][0]
				total_val = (max(_type1,_type2)+1)*10000 + (min(_type1,_type2)+1)*1000 + _type3 + ranks_inc
				ret.append(total_val)
				ret_names.append('two pair')
			ranks_inc += int(ranks_inc_amount)

			# three of a kind
			if (counter.most_common(1)[0][1] == 3):
				_type = counter.most_common(1)[0][0]
				rest_of_hand = set(self.ranks()) - set([_type])
				total_val = _type*(2**13) + ranks_inc + sum([2**rank for rank in rest_of_hand])
				ret.append(total_val)
				ret_names.append('three of a kind')
			ranks_inc += int(ranks_inc_amount)

			# flush
			if (counter2.most_common(1)[0][1] == 5):
				total_val = sum([2**rank for rank in self.ranks()]) + ranks_inc
				ret.append(total_val)
				ret_names.append('flush')
			ranks_inc += int(ranks_inc_amount)

			# full house
			if (counter.most_common(2)[0][1] == 3 and counter.most_common(2)[1][1] == 2):
				_type1 = counter.most_common(2)[0][0] 
				_type2 = counter.most_common(2)[1][0]
				total_val = (_type1+1)*1000 + _type2 + ranks_inc
				ret.append(total_val)
				ret_names.append('full house')
			ranks_inc += int(ranks_inc_amount)

			# four of a kind
			if (counter.most_common(1)[0][1] == 4):
				_type = counter.most_common(1)[0][0]
				rest_of_hand = set(self.ranks()) - set([_type])
				total_val = _type*(2**13) + ranks_inc + sum([2**rank for rank in rest_of_hand])
				ret.append(total_val)
				ret_names.append('four of a kind')
			ranks_inc += int(ranks_inc_amount)





		return (ret,ret_names)

	def ranking(self):
		return max(self.rankings()[0])