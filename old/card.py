class Card:

	ranks = { 0: "2", 1: "3", 2: "4", 3: "5", 4: "6", 5: "7", 6: "8", 7: "9", 8: "10", 9: "J", 10: "Q", 11: "K", 12: "A" }
	suits = { 0: "S", 1: "H", 2: "D", 3: "C" }

	def __init__(self, rank, suit):
		self.rank = rank
		self.suit = suit

	def __str__(self):
		suit_str = str(self.suits[self.suit])
		rank_str = str(self.ranks[self.rank])
		return rank_str + suit_str
		