from player import *
from deck import *
from hand import *

class Table:

	def __init__(self, players = None, deck = None):

		if (deck == None):
			self.deck = Deck(seed = 0)

		if (players == None):
			self.players = [Player() for i in xrange(5)]
			for p in self.players:
				p.draw(self.deck,2)
		self.hand = Hand()

	def __str__(self):
		ret = "\nTABLE info\n----------\n"
		ret += "deck: " + str(self.deck.count()) + "\n"
		for p in self.players:
			ret += str(p) + "\n"
		ret += "community cards -" + str(self.hand)
		return ret

	def draw(self,num_cards):
		for i in xrange(num_cards):
			self.hand.append(self.deck.draw())

	def flop(self):
		self.draw(3)

	def turn(self):
		self.draw(1)

	def river(self):
		self.draw(1)


