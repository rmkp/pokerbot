from card import *
import random

class Deck:

	ranks = { 0: "2", 1: "3", 2: "4", 3: "5", 4: "6", 5: "7", 6: "8", 7: "9", 8: "10", 9: "J", 10: "Q", 11: "K", 12: "A" }
	suites = { 0: "S", 1: "H", 2: "D", 3: "C" }

	d = {}

	def __init__(self, seed = None):

		if (seed != None):
			random.seed(seed)

		for rank in self.ranks:
			for suite in self.suites:
				self.d[suite*13 + rank] = Card(rank,suite)

	def __str__(self):
		out_str = 'deck: '
		for key in self.d:
			#if (key%13 == 0 and key > 0):
			#	out_str += '\n'
			out_str += str(self.d[key]) + ' '
		return out_str

	def draw(self):
		key = random.choice(self.d.keys())
		return self.d.pop(key, None)

	def count(self):
		return len(self.d)

